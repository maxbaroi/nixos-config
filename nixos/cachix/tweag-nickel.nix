
{
  nix = {
    settings = {
      substituters = [
        "https://tweag-nickel.cachix.org"
      ];
      trusted-public-keys = [
        "tweag-nickel.cachix.org-1:GIthuiK4LRgnW64ALYEoioVUQBWs0jexyoYVeLDBwRA="
      ];
    };
  };
}
